import type { SpotifyTrackId } from "./functions/data/spotify";

declare global {
  type SpotifyDeviceId = string;

  /** https://developer.spotify.com/documentation/web-playback-sdk/reference#webplaybackplayer-object#webplaybacktrack-object */
  type SpotifyPlaybackTrack = {
    id: SpotifyTrackId;
    type: "track" | "episode" | "ad";
    media_type: "audio" | "video";
    name: string;
    is_playable: boolean;
    album: {
      name: string;
      images: Array<{ url: string }>;
    };
    artists: Array<{ name: string }>;
  };

  /** https://developer.spotify.com/documentation/web-playback-sdk/reference#webplaybackplayer-object#webplaybackstate-object */
  type SpotifyPlaybackState = {
    position: number;
    loading: boolean;
    paused: boolean;
    duration: number;
    track_window: {
      current_track: SpotifyPlaybackTrack;
      previous_tracks: SpotifyPlaybackTrack[];
      next_tracks: SpotifyPlaybackTrack[];
    };
  };

  class SpotifyPlaybackController {
    constructor(params: {
      name: string;
      getOAuthToken: (callback: (token: string) => void) => void;
      volume?: number;
    });

    addListener(
      eventName: "ready",
      callback: (params: { device_id: SpotifyDeviceId }) => void,
    );
    addListener(
      eventName: "not_ready",
      callback: (params: { device_id: SpotifyDeviceId }) => void,
    );
    addListener(
      eventName: "player_state_changed",
      callback: (params: SpotifyPlaybackState) => void,
    );
    addListener(eventName: "autoplay_failed", callback: () => void);
    addListener(
      eventName: "authentication_error",
      callback: (error: { message: string }) => void,
    );
    addListener(
      eventName: "account_error",
      callback: (error: { message: string }) => void,
    );
    addListener(eventName: "initialization_error", callback: () => void);

    connect(): Promise<boolean>;
    disconnect();

    getCurrentState(): Promise<SpotifyPlaybackState | null>;

    pause();
    resume();
    togglePlay();
    previousTrack();
    nextTrack();
  }

  interface Window {
    onSpotifyWebPlaybackSDKReady?: () => void;

    Spotify: {
      Player: typeof SpotifyPlaybackController;
    };
  }
}

export {};
