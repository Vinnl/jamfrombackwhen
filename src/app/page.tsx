import { SessionProvider } from "next-auth/react";
import { auth, signIn } from "../auth";
import { Logo } from "../components/Logo";
import { Host } from "./Host";

export default async function Home() {
  const session = await auth();

  if (!session?.user) {
    return (
      <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
        <h1>
          <Logo className="max-w-full" />
        </h1>
        <form
          action={async () => {
            "use server";
            await signIn("spotify");
          }}
          className="flex flex-col items-center gap-5"
        >
          <button
            type="submit"
            className="bg-green-600 hover:bg-green-800 focus:outline focus:outline-4 focus:outline-green-600 hover:focus:outline-green-800 focus:outline-offset-4  rounded-2xl md:rounded-full px-10 py-5 text-2xl"
          >
            Connect to Spotify to host a game
          </button>
          <p className="text-lg">(Spotify Premium required.)</p>
        </form>
      </div>
    );
  }

  return (
    <SessionProvider session={session} refetchInterval={60}>
      <Host />
    </SessionProvider>
  );
}
