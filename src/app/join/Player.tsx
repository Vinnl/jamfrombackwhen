"use client";

import Peer, { DataConnection } from "peerjs";
import { FormEventHandler, useEffect, useMemo, useRef, useState } from "react";
import { v4 } from "uuid";
import { GameState, PlayerId, PlayerName } from "../../functions/gamestate";
import { PeerData } from "../../functions/data/peer";
import { ActiveGame } from "../../components/ActiveGame";
import { Logo } from "../../components/Logo";
import { LoadingAnimation } from "../../components/LoadingAnimation";

export const Player = () => {
  const peerRef = useRef<Peer>();
  const [hostConnection, setHostConnection] = useState<DataConnection | null>();
  const playerId = useMemo(() => v4(), []);
  const [gameState, setGameState] = useState<GameState | null>();
  const [playerNames, setPlayerNames] = useState<Record<PlayerId, PlayerName>>(
    {},
  );

  useEffect(() => {
    const peer = new Peer({
      debug: process.env.NODE_ENV === "development" ? 3 : undefined,
    });
    peer.on("error", (error) => {
      console.log("Guest error:", error);
    });
    const params = new URLSearchParams(document.location.hash?.substring(1));
    const hostId = params.get("game");
    if (typeof hostId === "string" && hostId.length > 0) {
      peer.on("open", () => {
        const connection = peer.connect(hostId);

        connection.on("open", () => {
          console.log("Opened, connected to host", connection);
          setHostConnection(connection);
        });
        connection.on("error", (error) => {
          console.log("Connection error", error);
        });
        connection.on("close", () => {
          console.log("Closing connection");
          setHostConnection(null);
        });
      });
    }

    peerRef.current = peer;

    return () => {
      peer.removeAllListeners();
    };
  }, []);

  useEffect(() => {
    if (!hostConnection) {
      return;
    }

    const onData = (d: unknown) => {
      const data = d as PeerData;

      if (data.type === "GameStateUpdate") {
        setGameState(data.value);
      }
      if (data.type === "PlayerRemoved") {
        setPlayerNames((prev) => {
          const newPlayerNames = { ...prev };
          delete newPlayerNames[data.value];
          return newPlayerNames;
        });
      }
    };

    hostConnection.on("data", onData);

    return () => {
      hostConnection.off("data", onData);
    };
  }, [hostConnection]);

  useEffect(() => {
    if (!hostConnection || Object.keys(playerNames).length === 0) {
      return;
    }

    const joinMessage: PeerData = {
      type: "PlayerJoin",
      value: {
        id: playerId,
        name: playerNames[playerId],
      },
    };
    hostConnection.send(joinMessage);

    return () => {
      const leaveMessage: PeerData = {
        type: "PlayerLeave",
        value: playerId,
      };
      hostConnection.send(leaveMessage);
    };
  }, [playerNames, hostConnection, playerId]);

  const leaveGame = () => {
    const leaveMessage: PeerData = {
      type: "PlayerLeave",
      value: playerId,
    };
    hostConnection?.send(leaveMessage);
    setPlayerNames((prev) => {
      const newPlayerNames = { ...prev };
      delete newPlayerNames[playerId];
      return newPlayerNames;
    });
  };

  if (hostConnection === null) {
    // TODO: Prettify
    return (
      <>
        <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
          <h1>
            <Logo className="max-w-full" />
          </h1>
          <p className="text-3xl">Game ended</p>
        </div>
      </>
    );
  }

  if (gameState && !gameState.players[playerId]) {
    return (
      <>
        <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
          <h1>
            <Logo className="max-w-full" />
          </h1>
          <ActiveGame gameState={gameState} />
        </div>
      </>
    );
  }

  if (
    // A game is in progress,
    gameState &&
    // we're listed as one of the players, but
    typeof gameState.players[playerId] !== "undefined" &&
    // we've removed the local record of our chosen name — i.e. we left an
    // in-progress game:
    Object.keys(playerNames).length === 0
  ) {
    return (
      <>
        <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
          <h1>
            <Logo className="max-w-full" />
          </h1>
          <ActiveGame
            gameState={gameState}
            playerId={playerId}
            onRejoinGame={() => {
              setPlayerNames((prev) => ({
                ...prev,
                [playerId]: gameState.players[playerId].name,
              }));
            }}
          />
        </div>
      </>
    );
  }

  if (Object.keys(playerNames).length === 0) {
    const joinGame: FormEventHandler = (event) => {
      event.preventDefault();
      const form = event.target as HTMLFormElement;
      const nameInput = form.elements.namedItem(
        "player-name",
      ) as HTMLInputElement;
      setPlayerNames((prev) => ({ ...prev, [playerId]: nameInput.value }));
    };
    return (
      <>
        <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
          <h1>
            <Logo className="max-w-full" />
          </h1>
          <form
            onSubmit={joinGame}
            className="max-w-full flex flex-col gap-5 justify-stretch"
          >
            <label htmlFor="player-name" className="text-3xl font-bold">
              Name:
            </label>
            <input
              id="player-name"
              type="text"
              placeholder="Saylor Twift"
              className="text-3xl font-bold text-slate-800 placeholder:text-slate-300 bg-white focus:outline focus:outline-2 focus:outline-orange-300 focus:outline-offset-2 p-5 rounded-lg"
            />
            <button
              type="submit"
              className="text-3xl font-bold bg-orange-600 text-yellow-200 hover:bg-orange-500 hover:-rotate-2 hover:shadow-xl focus:outline focus:outline-2 focus:outline-orange-300 focus:outline-offset-2 p-5 rounded-lg"
            >
              Join game
            </button>
          </form>
        </div>
      </>
    );
  }

  if (!hostConnection || !gameState) {
    return (
      <>
        <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
          <h1>
            <Logo className="max-w-full" />
          </h1>
          <div className="flex flex-col gap-5">
            <p className="text-3xl">
              {gameState === null ? (
                <>
                  The host stopped the game. Waiting for new game to
                  start&hellip;
                </>
              ) : (
                <>Waiting for game to start&hellip;</>
              )}
            </p>
            <button
              onClick={() => leaveGame()}
              className="text-lg underline cursor-pointer hover:text-orange-500"
            >
              Leave game
            </button>
            <LoadingAnimation />
          </div>
        </div>
      </>
    );
  }

  const sendMessage = (message: PeerData) => {
    hostConnection.send(message);
  };

  return (
    <>
      <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
        <h1>
          <Logo className="max-w-full" />
        </h1>
        <ActiveGame
          gameState={gameState}
          playerId={playerId}
          onPositionGuess={(positionGuess) =>
            sendMessage({
              type: "Guess",
              value: {
                playerId: playerId,
                round: gameState.completedRound + 1,
                positionGuess: positionGuess,
              },
            })
          }
          onPlayAgain={() => {
            return sendMessage({ type: "PlayAgain" });
          }}
          onQuitGame={() => leaveGame()}
        />
      </div>
    </>
  );
};
