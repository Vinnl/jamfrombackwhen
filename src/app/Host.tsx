"use client";

import Peer, { DataConnection } from "peerjs";
import { FormEventHandler, useEffect, useId, useRef, useState } from "react";
import { signOut, useSession } from "next-auth/react";
import { MdRemove } from "react-icons/md";
import { QRCodeSVG } from "qrcode.react";
import { FaSpinner } from "react-icons/fa6";
import styles from "./Host.module.css";
import { SpotifyPlayer } from "../components/SpotifyPlayer";
import {
  GameState,
  Player,
  PlayerId,
  PlayerName,
  getCollectiveTimelineAtRound,
  getCurrentPlayerId,
  getCurrentTrack,
  getLastCorrectTrack,
  markPlayerAsDisconnected,
  markPlayerAsRejoined,
  updateWithGuess,
} from "../functions/gamestate";
import { PeerData } from "../functions/data/peer";
import {
  SpotifyGetPlaylistTracksResponseBody,
  SpotifyPlaylistId,
  fetchPlaylistTracks,
  getAlbumYear,
} from "../functions/data/spotify";
import { APP_URL } from "../constants";
import { Logo } from "../components/Logo";
import { LoadingAnimation } from "../components/LoadingAnimation";
import { Timeline } from "../components/Timeline";
import { GameStats } from "../components/GameStats";
import { ActiveGame } from "../components/ActiveGame";
import { PlaylistData, PlaylistPicker } from "../components/PlaylistPicker";

type PeerId = Peer["id"];
type CandidateTracks = SpotifyGetPlaylistTracksResponseBody["items"];
type JoinedPlayer = { name: PlayerName; peerId: PeerId };

export type Props = {};

const listFormatter = new Intl.ListFormat("en", { style: "short" });
const HOST_PLAYER_ID: PlayerId = "host";

export const Host = (props: Props) => {
  const session = useSession();
  const peerRef = useRef<Peer>();
  const [gameId, setGameId] = useState<PeerId>();
  const [players, setPlayers] = useState<Record<PlayerId, JoinedPlayer>>({});
  const [connections, setConnections] = useState<DataConnection[]>([]);
  const [gameState, setGameState] = useState<GameState>();
  const [chosenPlaylist, setChosenPlaylist] = useState<PlaylistData>();
  const [playlistTracks, setPlaylistTracks] =
    useState<SpotifyGetPlaylistTracksResponseBody["items"]>();
  const [candidateTracks, setCandidateTracks] = useState<CandidateTracks>();
  const [triggeredStart, setTriggeredStart] = useState(false);
  const [hasInitialisationError, setHasInitialisationError] = useState(false);
  const [gameNr, setGameNr] = useState(0);

  useEffect(() => {
    const peer = new Peer({
      debug: process.env.NODE_ENV === "development" ? 3 : undefined,
    });
    peer.on("open", (id) => {
      setGameId(id);
    });

    peer.on("error", (error) => {
      console.log("Host error:", error);
    });

    peer.on("connection", (connection) => {
      function sendMessage(data: PeerData) {
        connection.send(data);
      }

      connection.on("open", () => {
        setConnections((prevConnections) => [...prevConnections, connection]);
      });

      connection.on("data", (d) => {
        const data = d as PeerData;
        if (data.type === "PlayerJoin") {
          setPlayers((prev) => ({
            ...prev,
            [data.value.id]: {
              name: data.value.name,
              peerId: connection.peer,
            },
          }));
          setGameState((prev) => {
            if (!prev) {
              // Game hasn't started yet, no worries:
              return;
            }
            return markPlayerAsRejoined(prev, data.value.id);
          });
        } else if (data.type === "PlayerLeave") {
          setPlayers((prevPlayers) => {
            const newPlayers = { ...prevPlayers };
            delete newPlayers[data.value];
            return newPlayers;
          });
          setGameState((prev) => {
            if (!prev) {
              // Game hasn't started yet, no worries:
              return;
            }
            return markPlayerAsDisconnected(prev, data.value);
          });
        } else if (data.type === "Guess") {
          // TODO: Check if the player and round are in sync with the game state?
          setGameState((prev) => {
            if (!prev) {
              console.error(
                "Trying to make a guess when the game hasn't started yet.",
              );
              return;
            }
            return updateWithGuess(prev, data.value.positionGuess);
          });
        } else if (data.type === "PlayAgain") {
          setGameNr((prev) => prev + 1);
        }
      });

      connection.on("close", () => {
        // TODO: Mark player as inactive and skip their turn?
        setConnections((prevConnections) =>
          prevConnections.filter(
            (knownConnection) => knownConnection.peer !== connection.peer,
          ),
        );
        setPlayers((prev) => {
          return Object.fromEntries(
            Object.entries(prev).filter(
              ([_playerId, player]) => player.peerId !== connection.peer,
            ),
          );
        });
        setGameState((prev) => {
          if (!prev) {
            // Game hasn't started yet, no worries:
            return;
          }
          const disconnectedPlayer = Object.keys(prev.players).find(
            (playerId) => prev.players[playerId].peerId === connection.peer,
          );
          if (typeof disconnectedPlayer === "undefined") {
            return;
          }
          return markPlayerAsDisconnected(prev, disconnectedPlayer);
        });
      });
    });

    peerRef.current = peer;
  }, []);

  useEffect(() => {
    if (!gameState) {
      return;
    }

    connections.forEach((connection) => {
      const stateMessage: PeerData = {
        type: "GameStateUpdate",
        value: gameState,
      };
      connection.send(stateMessage);
    });
  }, [connections, gameState]);

  useEffect(() => {
    setCandidateTracks(undefined);
    setPlaylistTracks(undefined);

    if (!chosenPlaylist || !session.data?.accessToken) {
      return;
    }

    fetchPlaylistTracks(chosenPlaylist.id, session.data.accessToken).then(
      (playlistTracks) => {
        setPlaylistTracks(playlistTracks.items);
      },
    );
  }, [chosenPlaylist, session.data?.accessToken]);

  useEffect(() => {
    if (!triggeredStart || !candidateTracks || !chosenPlaylist) {
      return;
    }
    const nrOfPlayers = Object.keys(players).length;
    const seeds = candidateTracks.slice(0, nrOfPlayers);
    const targetTracks = candidateTracks.slice(nrOfPlayers);
    const shuffledPlayerIds = Object.keys(players);
    shuffledPlayerIds.sort(() => Math.random() - 0.5);
    const initialGameState: GameState = {
      started: true,
      players: Object.fromEntries(
        shuffledPlayerIds.map((playerId, playerIndex) => {
          const player: Player = {
            name: players[playerId].name,
            isPresent: true,
            seed: {
              year: getAlbumYear(seeds[playerIndex].track.album),
              spotifyId: seeds[playerIndex].track.id,
              artist: listFormatter.format(
                seeds[playerIndex].track.artists.map((artist) => artist.name),
              ),
              title: seeds[playerIndex].track.name,
            },
            peerId: players[playerId].peerId,
          };
          return [playerId, player];
        }),
      ),
      completedRound: -1,
      responses: [],
      tracks: targetTracks.map((targetTrack) => ({
        year: getAlbumYear(targetTrack.track.album),
        spotifyId: targetTrack.track.id,
        artist: listFormatter.format(
          targetTrack.track.artists.map((artist) => artist.name),
        ),
        title: targetTrack.track.name,
      })),
      playlist: chosenPlaylist,
    };
    setGameState(initialGameState);
    setTriggeredStart(false);
  }, [candidateTracks, chosenPlaylist, players, triggeredStart]);

  useEffect(() => {
    if (playlistTracks) {
      setCandidateTracks(shuffleTracks(playlistTracks));
    }
    if (gameNr > 0) {
      startGame();
    }
  }, [gameNr, playlistTracks]);

  if (!session.data) {
    // This should've been caught by `page.tsx`
    return null;
  }

  if (session.data.error === "RefreshAccessTokenError") {
    // TODO: Do something?
    signOut();
  }

  if (!session.data.accessToken) {
    session.update();
    return <></>;
  }

  async function startGame() {
    setTriggeredStart(true);
  }

  async function endGame() {
    setGameState(undefined);
    connections.forEach((connection) => {
      const stateMessage: PeerData = {
        type: "GameStateUpdate",
        value: null,
      };
      connection.send(stateMessage);
    });
    if (playlistTracks) {
      setCandidateTracks(shuffleTracks(playlistTracks));
    }
  }

  return (
    <>
      <SpotifyPlayer
        authToken={session.data.accessToken}
        currentTrack={
          gameState ? getCurrentTrack(gameState)?.spotifyId : undefined
        }
        onAuthError={async () => {
          const newSession = await session.update();
          if (newSession === null) {
            signOut();
          }
        }}
        onInitialisationError={() => setHasInitialisationError(true)}
      />

      <div className="min-h-full flex flex-col gap-5 p-5">
        <header className="flex justify-center p-5">
          <h1>
            <Logo className="max-w-full" />
          </h1>
        </header>
        <main className="flex-grow">
          {hasInitialisationError ? (
            <div className="flex flex-col items-center justify-center py-10">
              <div className="w-96 max-w-full bg-red-300 text-slate-800 text-lg p-5 rounded">
                Unable to play music. You might need to enable DRM.
              </div>
            </div>
          ) : typeof gameId === "undefined" ? (
            <LoadingAnimation />
          ) : typeof gameState === "undefined" ? (
            <div className="flex flex-col gap-10 md:gap-5">
              <PlaylistPicker
                accessToken={session.data.accessToken}
                chosenPlaylist={chosenPlaylist}
                onPick={setChosenPlaylist}
                profile={session.data.profile}
              />
              <Lobby
                gameId={gameId}
                joinedPlayers={players}
                isStarting={
                  triggeredStart && typeof playlistTracks === "undefined"
                }
                onStart={() => startGame()}
                onLocalJoin={(name) => {
                  setPlayers((prev) => ({
                    ...prev,
                    [HOST_PLAYER_ID]: {
                      name: name,
                      peerId: gameId,
                    },
                  }));
                }}
                onRemovePlayer={(playerId) => {
                  const playerConnection = connections.find(
                    (connection) =>
                      connection.peer === players[playerId].peerId,
                  );
                  const removePlayerMessage: PeerData = {
                    type: "PlayerRemoved",
                    value: playerId,
                  };
                  playerConnection?.send(removePlayerMessage);
                  setPlayers((prev) => {
                    const newPlayers = { ...prev };
                    delete newPlayers[playerId];
                    return newPlayers;
                  });
                }}
              />
            </div>
          ) : (
            <div className="flex flex-col items-center justify-center min-h-full p-5 gap-10 md:p-10 md:gap-16">
              {typeof gameState.players[HOST_PLAYER_ID] !== "undefined" ? (
                <ActiveGame
                  gameState={gameState}
                  playerId={HOST_PLAYER_ID}
                  onPositionGuess={(positionGuess) => {
                    setGameState((prev) => {
                      if (!prev) {
                        console.error(
                          "Trying to make a guess when the game hasn't started yet.",
                        );
                        return;
                      }
                      return updateWithGuess(prev, positionGuess);
                    });
                  }}
                  onPlayAgain={() => setGameNr((prev) => prev + 1)}
                  onEndGame={() => endGame()}
                />
              ) : (
                <>
                  <GameStats
                    gameState={gameState}
                    onPlayAgain={() => setGameNr((prev) => prev + 1)}
                    onEndGame={() => endGame()}
                  />
                  <Timeline
                    timeline={getCollectiveTimelineAtRound(
                      gameState,
                      gameState.completedRound,
                    )}
                    lastAdded={getLastCorrectTrack(gameState)?.spotifyId}
                  />
                </>
              )}
            </div>
          )}
        </main>
        <footer>
          <p className="text-end p-5">
            Connected to the Spotify account{" "}
            <a
              href={`https://open.spotify.com/user/${session.data.user.name}`}
              target="_blank"
              className="font-bold"
            >
              {session.data.user.name}
            </a>
            .{" "}
            <button
              onClick={() => signOut()}
              className="underline hover:text-orange-600"
            >
              Disconnect.
            </button>
          </p>
        </footer>
      </div>
    </>
  );
};

type LobbyProps = {
  gameId: PeerId;
  joinedPlayers: Record<PlayerId, JoinedPlayer>;
  isStarting: boolean;
  onStart: () => void;
  onLocalJoin: (playerName: string) => void;
  onRemovePlayer: (playerId: PlayerId) => void;
};

const Lobby = (props: LobbyProps) => {
  const joinLinkRef = useRef<HTMLInputElement>(null);
  const joinLinkId = useId();
  const joinUrl = `${APP_URL}/join#game=${props.gameId}`;
  const [showLocalJoinForm, setShowLocalJoinForm] = useState(false);

  const copy = () => {
    navigator.clipboard.writeText(joinUrl);
  };

  const nrOfPlayers = Object.keys(props.joinedPlayers).length;

  const startLink =
    nrOfPlayers < 1 ? null : (
      <>
        <button
          className="text-3xl font-bold bg-orange-600 text-yellow-200 hover:bg-orange-500 hover:-rotate-2 hover:shadow-xl focus:outline focus:outline-2 focus:outline-orange-300 focus:outline-offset-2 p-5 rounded-lg"
          onClick={() => props.onStart()}
        >
          {props.isStarting ? (
            <FaSpinner className="animate-spin" aria-label="Loading…" />
          ) : (
            <>Start game!</>
          )}
        </button>
      </>
    );

  return (
    <div className={`${styles.fadeIn} flex flex-col items-center gap-10`}>
      <div className="flex flex-col items-center gap-5 px-5 md:py-5 text-center">
        <label
          htmlFor={joinLinkId}
          className="flex flex-col gap-5 cursor-pointer"
        >
          <span className="text-2xl">Share this link to join:</span>
          <span className="inline-block rounded-lg p-5 bg-yellow-200 hover:bg-yellow-100">
            <QRCodeSVG
              value={joinUrl}
              size={256}
              bgColor="transparent"
              fgColor="inherit"
              className="fill-orange-600"
            />
          </span>
        </label>
        <input
          id={joinLinkId}
          value={joinUrl}
          readOnly
          ref={joinLinkRef}
          className="p-5 bg-yellow-50 hover:bg-yellow-100 cursor-pointer rounded-lg border-2 border-yellow-500 text-yellow-800 text-xl max-w-full box-content"
          style={{ width: "256px" }}
          onClick={(event) => {
            joinLinkRef.current!.select();
            copy();

            if (event.altKey) {
              // Loop hole to make testing easier:
              window.open(joinUrl, "_blank");
            }
          }}
        />
      </div>
      <div className="flex flex-col items-center gap-5 px-5 md:py-5 text-center">
        <h2 className="text-2xl">
          {nrOfPlayers === 0 ? (
            <>
              Waiting for players to join
              <span className="animate-pulse">&hellip;</span>
            </>
          ) : (
            <>Players</>
          )}
        </h2>
        <ul className="flex flex-wrap justify-center gap-4 text-lg">
          {Object.entries(props.joinedPlayers).map(([playerId, player]) => {
            return (
              <li
                key={playerId}
                className="flex items-center gap-2 text-2xl font-bold border-4 border-solid border-white rounded"
              >
                <span className="p-2">{player.name}</span>
                <button
                  className="bg-white text-slate-800 font-bold self-stretch p-2 hover:bg-red-400 hover:text-white"
                  onClick={() => props.onRemovePlayer(playerId)}
                >
                  <MdRemove aria-label={`Remove player: ${player.name}`} />
                </button>
              </li>
            );
          })}
          {!showLocalJoinForm &&
            typeof props.joinedPlayers[HOST_PLAYER_ID] === "undefined" && (
              <li>
                <button
                  onClick={() => setShowLocalJoinForm(true)}
                  className="text-2xl border-2 border-dashed hover:bg-white hover:text-slate-800 focus:border-solid focus:outline-none border-white p-2 rounded"
                >
                  Join on this device
                </button>
              </li>
            )}
        </ul>
        {showLocalJoinForm &&
          typeof props.joinedPlayers[HOST_PLAYER_ID] === "undefined" && (
            <LocalJoinForm
              onPickName={(name) => {
                props.onLocalJoin(name);
                setShowLocalJoinForm(false);
              }}
            />
          )}
        {startLink}
      </div>
    </div>
  );
};

const LocalJoinForm = (props: { onPickName: (name: string) => void }) => {
  const onSubmit: FormEventHandler = (event) => {
    event.preventDefault();
    const form = event.target as HTMLFormElement;
    const nameInput = form.elements.namedItem(
      "player-name",
    ) as HTMLInputElement;
    props.onPickName(nameInput.value);
  };

  return (
    <form
      onSubmit={onSubmit}
      className="flex flex-col text-xl text-slate-700 bg-yellow-300 max-w-full w-96 border-4 border-solid border-yellow-300 rounded-md p-2 gap-5"
    >
      <label htmlFor="player-name" className="font-bold text-start">
        Join as:
      </label>
      <input
        id="player-name"
        type="text"
        placeholder="Saylor Twift"
        className="font-bold text-slate-800 placeholder:text-slate-300 bg-white focus:outline focus:outline-2 focus:outline-orange-300 focus:outline-offset-2 p-2 rounded-lg"
        autoFocus
      />
      <button
        type="submit"
        className="font-bold bg-orange-600 text-yellow-200 hover:bg-orange-500 hover:shadow-xl focus:outline focus:outline-2 focus:outline-orange-300 focus:outline-offset-2 p-2 rounded-lg"
      >
        Join game
      </button>
    </form>
  );
};

function shuffleTracks(
  playlistTracks: SpotifyGetPlaylistTracksResponseBody["items"],
): SpotifyGetPlaylistTracksResponseBody["items"] {
  const shuffledTracks = playlistTracks.filter(
    // TODO: For tracks from compilation albums, find the original album?
    (item) =>
      item.track !== null &&
      !item.is_local &&
      item.track.album.album_type !== "compilation",
  );
  shuffledTracks.sort(() => Math.random() - 0.5);
  return shuffledTracks;
}
