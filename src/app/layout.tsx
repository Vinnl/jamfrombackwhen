import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Jam from back when",
  description: "A musical party game for all ages",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="min-h-full">
      <body
        className={`${inter.className} min-h-full bg-slate-700 text-white selection:bg-yellow-300 selection:text-orange-700`}
      >
        {children}
      </body>
    </html>
  );
}
