import NextAuth, {
  type Account,
  type DefaultSession,
  type Session,
} from "next-auth";
import type { JWT } from "next-auth/jwt";
import Spotify from "next-auth/providers/spotify";
import {
  SpotifyGetProfileResponseBody,
  rotateRefreshToken,
} from "./functions/data/spotify";

declare module "next-auth" {
  /**
   * Returned by `auth`, `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    accessToken: Account["access_token"] | null;
    expiresAt: JWT["expiresAt"] | null;
    user: {
      /**
       * By default, TypeScript merges new interface properties and overwrites existing ones.
       * In this case, the default session user properties will be overwritten,
       * with the new ones defined above. To keep the default session user properties,
       * you need to add them back into the newly declared interface.
       */
    } & DefaultSession["user"];
    profile: SpotifyGetProfileResponseBody;
    error?: "RefreshAccessTokenError";
  }
}

declare module "next-auth/jwt" {
  /** Returned by the `jwt` callback and `auth`, when using JWT sessions */
  interface JWT {
    accessToken: Account["access_token"];
    refreshToken: Account["refresh_token"];
    expiresAt: Account["expires_at"];
    profile: SpotifyGetProfileResponseBody;
    error?: "RefreshAccessTokenError";
  }
}

const scopes = [
  "user-read-email",
  "streaming",
  "user-read-private",
  "user-modify-playback-state",
];
export const { handlers, signIn, signOut, auth } = NextAuth({
  providers: [
    Spotify({
      authorization: `https://accounts.spotify.com/authorize?scope=${scopes.join(encodeURIComponent(" "))}`,
    }),
  ],

  callbacks: {
    // https://authjs.dev/reference/core#jwt
    async jwt({ token, account, user, profile, session }) {
      if (profile) {
        token.profile = profile as SpotifyGetProfileResponseBody;
      }
      if (account) {
        // `account` is only set when `trigger` is "signIn" or "signUp":
        token.accessToken = account.access_token;
        token.refreshToken = account.refresh_token;
        token.expiresAt = account.expires_at;
      } else {
        // This is not the first login (because `account` is undefined):
        if (
          typeof token.expiresAt === "number" &&
          typeof token.refreshToken === "string" &&
          Date.now() > token.expiresAt * 1000 - 5 * 60 * 1000
        ) {
          // The access token is about to expire (in max. 5 minutes), and we
          // have a refresh token. Also see:
          // https://authjs.dev/guides/refresh-token-rotation
          try {
            const rotatedTokens = await rotateRefreshToken(token.refreshToken);
            token.accessToken = rotatedTokens.accessToken;
            token.refreshToken = rotatedTokens.refreshToken;
            token.expiresAt = rotatedTokens.expiresAt;
          } catch (e) {
            token.error = "RefreshAccessTokenError";
          }
        }
      }
      return token;
    },
    // https://authjs.dev/reference/core#session
    session({ session, token }) {
      session.accessToken = token.accessToken ?? null;
      session.expiresAt = token.expiresAt;
      session.error = token.error;
      session.profile = token.profile;
      return session;
    },
  },
});
