export const WINNING_TIMELINE_LENGTH = 10;
export const SUGGESTED_PLAYLIST_IDS =
  process.env.NEXT_PUBLIC_SUGGESTED_PLAYLIST_IDS ??
  "6YlB8RiNTzuauYKlR0zg11,37i9dQZF1DX5KpP2LN299J,37i9dQZF1DX5KpP2LN299J";
export const APP_URL =
  process.env.NEXT_PUBLIC_APP_URL ?? "https://jamfrombackwhen.vincenttunru.com";
