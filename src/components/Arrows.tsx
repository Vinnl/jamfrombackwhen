import { SVGProps } from "react";

export const ArrowDown = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3.7292037mm"
      height="12.670896mm"
      viewBox="0 0 3.7292037 12.670896"
      version="1.1"
      {...props}
    >
      <g>
        <g
          strokeLinecap="round"
          transform="matrix(.26458 0 0 .26458 -2.38 -2.382) translate(10.607 10.445)"
        >
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M.04-.44c.6 3.88 1.73 15.84 3.55 23.39 1.81 7.56 6.02 18.29 7.34 21.95M-.61.52c.78 4 3.36 15.62 5.22 23.11 1.86 7.49 5 18.19 5.93 21.82"
            ></path>
          </g>
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M3.87 36.03c1.48 2.36 3.87 5.5 6.67 9.42m-6.67-9.42c2.18 2.65 3.97 5.59 6.67 9.42"
            ></path>
          </g>
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M11.48 33.95c-.78 2.97-.64 6.73-.94 11.5m.94-11.5c.05 3.23-.31 6.76-.94 11.5"
            ></path>
          </g>
        </g>
      </g>
    </svg>
  );
};
export const ArrowUp = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12.74"
      height="58.75"
      viewBox="0 0 3.371 15.544"
      version="1.1"
      {...props}
    >
      <g>
        <g
          strokeLinecap="round"
          transform="matrix(.26458 0 0 .26458 -2.21 -2.38) translate(17.813 66.989)"
        >
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M.73-.24C-.73-4.48-7.81-15.75-7.81-24.9c0-9.14 7.17-25.16 8.53-30.21M-.35-1.42c-1.15-4.06-5.2-12.62-5.18-21.89.03-9.26 4.37-28.01 5.34-33.68"
            ></path>
          </g>
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M2.28-41.17C.96-48.19.5-53.42-.19-56.99m2.47 15.82C2.14-47.77.04-52.68-.19-56.99"
            ></path>
          </g>
          <g>
            <path
              fill="none"
              strokeWidth="2"
              d="M-8.46-43.28c2.73-6.29 6.3-10.73 8.27-13.71m-8.27 13.71c3.87-5.89 5.78-10.01 8.27-13.71"
            ></path>
          </g>
        </g>
      </g>
    </svg>
  );
};
