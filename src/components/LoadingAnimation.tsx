import { IoIosMusicalNote, IoIosMusicalNotes } from "react-icons/io";
import styles from "./LoadingAnimation.module.css";
import { useEffect, useMemo, useState } from "react";

export const LoadingAnimation = () => {
  const [isReadyToRender, setIsReadyToRender] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      // Only render if whatever we're waiting on hasn't already loaded within 200ms
      setIsReadyToRender(true);
    }, 200);
  }, []);
  const musicalNotes = useMemo(() => {
    const numberOfNotes = getIntBetween(50, 70);
    const fillColours = [
      "text-orange-200",
      "text-orange-300",
      "text-orange-400",
      "text-orange-500",
      "text-orange-600",
      "text-yellow-200",
      "text-yellow-300",
      "text-yellow-400",
      "text-yellow-500",
    ];
    return [...new Array(numberOfNotes)].map((_v, index) => {
      const xPos = getIntBetween(5, 95);
      const yPos = getIntBetween(5, 95);
      const delay = getIntBetween(-120, 120);
      const size = getIntBetween(50, 100);
      const legs = getIntBetween(1, 3);
      const fillColour = fillColours[getIntBetween(0, fillColours.length)];
      const Icon = legs === 1 ? IoIosMusicalNote : IoIosMusicalNotes;

      return (
        <Icon
          key={`note_${index}`}
          className={`${styles.pulse} absolute ${fillColour}`}
          size={size}
          style={{
            insetInlineStart: `${xPos}%`,
            insetBlockStart: `${yPos}%`,
            animationDelay: `${delay}s`,
          }}
        />
      );
    });
  }, []);

  return (
    <div className="w-full flex justify-center">
      <div className="w-full md:w-1/2 h-96 relative overflow-clip">
        {isReadyToRender ? musicalNotes : null}
      </div>
    </div>
  );
};

const getIntBetween = (min: number, max: number): number => {
  const minCeiled = Math.ceil(min);
  const maxFloored = Math.floor(max);
  return Math.floor(Math.random() * (maxFloored - minCeiled) + minCeiled);
};
