import {
  CSSProperties,
  FormEventHandler,
  memo,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { Account, Session } from "next-auth";
import { FaSearch } from "react-icons/fa";
import {
  SpotifyGetSearchResponseBody,
  SpotifyImageObjectArray,
  SpotifyPlaylistId,
  fetchPlaylistData,
  fetchSearchResults,
} from "../functions/data/spotify";
import { SUGGESTED_PLAYLIST_IDS, WINNING_TIMELINE_LENGTH } from "../constants";

export type PlaylistData = {
  name: string;
  id: SpotifyPlaylistId;
  images: SpotifyImageObjectArray;
};

type CountryCode = "NL" | string;

export type Props = {
  onPick: (data: PlaylistData) => void;
  accessToken: Account["access_token"];
  profile?: Session["profile"];
  chosenPlaylist?: PlaylistData;
};

const suggestedPlaylistsByCountry: Record<CountryCode, SpotifyPlaylistId[]> = {
  NL: [
    // Top 2000
    "37i9dQZF1DWTmvXBN4DgpA",
    // Q-Music Foute 1500
    "1SutuoTknFbxL9jVsUxbF2",
    // 80s, 90s, 00s Greatest Hits
    "6YlB8RiNTzuauYKlR0zg11",
  ],
  US: [
    // Rolling Stone Magazine — 500 greatest songs of all time
    "7EAqBCOVkDZcbccjxZmgjp",
    // This is Taylor Swift
    "37i9dQZF1DX5KpP2LN299J",
    // 80s, 90s, 00s Greatest Hits
    "6YlB8RiNTzuauYKlR0zg11",
    // Every Billboard Hot 100 #1 Hit
    // "0qniWtqm26thk3NRi11zLD",
  ],
};

export const PlaylistPicker = memo((props: Props) => {
  const [suggestedPlaylistData, setSuggestedPlaylistData] = useState<
    PlaylistData[]
  >([]);
  const [searchResults, setSearchResults] = useState<PlaylistData[]>();
  const [isExpanded, setIsExpanded] = useState(false);
  const onPick = useCallback<Props["onPick"]>(
    (data) => {
      props.onPick(data);
      setIsExpanded(false);
    },
    [props],
  );
  const suggestedPlaylistIds = useMemo(() => {
    const searchParams = new URLSearchParams(document.location.search);
    const playlistIdParam = searchParams.get("playlist");
    const suggestedPlaylists =
      suggestedPlaylistsByCountry[props.profile?.country ?? "default"] ??
      SUGGESTED_PLAYLIST_IDS.split(",");
    if (typeof playlistIdParam === "string") {
      return [encodeURIComponent(playlistIdParam), ...suggestedPlaylists];
    }
    return suggestedPlaylists;
  }, [props.profile]);

  useEffect(() => {
    const [defaultPlaylistId, ...otherSuggestedPlaylistIds] =
      suggestedPlaylistIds;
    fetchPlaylistData(defaultPlaylistId, props.accessToken).then(
      async (data) => {
        const playlistMetadata: PlaylistData = {
          id: data.id,
          images: data.images,
          name: data.name,
        };
        setSuggestedPlaylistData([playlistMetadata]);
        if (typeof props.chosenPlaylist === "undefined") {
          onPick(playlistMetadata);
        }

        const otherSuggestedPlaylistData = await Promise.all(
          otherSuggestedPlaylistIds.map((playlistId) => {
            return fetchPlaylistData(playlistId, props.accessToken);
          }),
        );
        setSuggestedPlaylistData([
          playlistMetadata,
          ...otherSuggestedPlaylistData,
        ]);
      },
    );
  }, [onPick, props.accessToken, props.chosenPlaylist, suggestedPlaylistIds]);

  const queryFieldId = "playlist-search-query";
  const onSubmitSearch: FormEventHandler = async (event) => {
    event.preventDefault();
    const form = event.target as HTMLFormElement;
    const nameInput = form.elements.namedItem(queryFieldId) as HTMLInputElement;
    if (nameInput.value === "") {
      setSearchResults(undefined);
      return;
    }
    const results = await fetchSearchResults(
      nameInput.value,
      ["playlist"],
      props.accessToken,
    );
    const resultsAsPlaylistData: PlaylistData[] = results.playlists.items
      .filter((searchResults) => {
        return searchResults.tracks.total >= WINNING_TIMELINE_LENGTH * 5;
      })
      .map((searchResult) => {
        return {
          id: searchResult.id,
          images: searchResult.images,
          name: searchResult.name,
        };
      });
    setSearchResults(resultsAsPlaylistData);
  };

  const availablePlaylists =
    searchResults?.slice(0, 3) ?? suggestedPlaylistData;

  const horizontalExpansionDuration = "200ms";
  const verticalExpansionDuration = "400ms";

  return (
    <>
      <section className={`flex flex-col items-center`}>
        <div
          className={`${isExpanded ? "w-full bg-yellow-200 text-slate-800" : "w-96"} max-w-full md:max-w-screen-md flex flex-col items-center px-3 py-2 border border-solid border-white rounded-lg`}
          style={{
            transition: `width ${horizontalExpansionDuration}, background-color 0ms, color 0ms`,
            transitionDelay: isExpanded ? undefined : "400ms",
          }}
        >
          <button
            className="flex justify-between items-center w-full px-1 font-bold hover:underline"
            onClick={() => setIsExpanded(!isExpanded)}
          >
            {typeof props.chosenPlaylist === "undefined" ? (
              <>Loading playlist&hellip;</>
            ) : (
              <span>Playlist: {props.chosenPlaylist.name}</span>
            )}
          </button>
          <div
            // https://css-tricks.com/css-grid-can-do-auto-height-transitions/
            style={{
              display: "grid",
              gridTemplateRows: isExpanded ? "1fr" : "0fr",
              overflow: "hidden",
              transition: `grid-template-rows ${verticalExpansionDuration}`,
              transitionDelay: isExpanded
                ? horizontalExpansionDuration
                : undefined,
            }}
          >
            <div
              // https://css-tricks.com/css-grid-can-do-auto-height-transitions/
              style={{
                minHeight: 0,
                transition: `visibility ${verticalExpansionDuration}`,
                transitionDelay: isExpanded
                  ? horizontalExpansionDuration
                  : undefined,
                visibility: isExpanded ? "visible" : "hidden",
              }}
            >
              <div className="py-5 px-1 flex flex-col gap-3">
                <form
                  onSubmit={onSubmitSearch}
                  className="flex items-center gap-3 flex-wrap"
                >
                  <label htmlFor={queryFieldId} className="">
                    Find a playlist:
                  </label>
                  <div className="flex-grow flex items-center gap-2">
                    <input
                      type="search"
                      name={queryFieldId}
                      id={queryFieldId}
                      className="flex-grow p-2 rounded focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-orange-500"
                      placeholder="Top 1000 Disco Hits"
                    />
                    <button
                      type="submit"
                      className="flex-shrink-0 p-3 rounded hover:bg-yellow-300 focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-orange-500"
                    >
                      <FaSearch aria-label="Find" />
                    </button>
                  </div>
                </form>
                <PlaylistGrid
                  availablePlaylists={availablePlaylists}
                  onPick={(pickedPlaylist) => onPick(pickedPlaylist)}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
});
PlaylistPicker.displayName = "PlaylistPicker";

type PlaylistGridProps = {
  availablePlaylists: PlaylistData[];
  onPick: (playlist: PlaylistData) => void;
};
const PlaylistGrid = (props: PlaylistGridProps) => {
  return (
    <>
      <ul className="flex flex-wrap justify-stretch items-center gap-5">
        {props.availablePlaylists.map((playlist) => {
          return (
            <li
              key={playlist.id}
              className="basis-56 flex-grow flex justify-center items-start self-stretch"
            >
              <button
                onClick={() => props.onPick(playlist)}
                className="flex-grow flex flex-col justify-center items-center gap-2 p-5 rounded-lg hover:underline hover:bg-yellow-300"
              >
                <PlaylistImage playlist={playlist} />
                <b>{playlist.name}</b>
              </button>
            </li>
          );
        })}
      </ul>
    </>
  );
};

type PlaylistImageProps = {
  playlist: PlaylistData;
};
const PlaylistImage = (props: PlaylistImageProps) => {
  if (
    !Array.isArray(props.playlist.images) ||
    props.playlist.images.length === 0
  ) {
    return (
      <span
        role="presentation"
        className="bg-yellow-400 flex justify-center items-center text-xl rounded-3xl w-36 h-36"
      >
        {props.playlist.name}
      </span>
    );
  }

  return (
    // eslint-disable-next-line @next/next/no-img-element
    <img
      src={props.playlist.images[0]?.url}
      alt=""
      className="rounded-3xl w-36 h-36"
    />
  );
};
