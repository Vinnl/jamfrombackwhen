import { useEffect, useState } from "react";
import { Comic_Neue } from "next/font/google";
import { PlayerTimeline } from "../functions/gamestate";
import { ArrowDown, ArrowUp } from "./Arrows";
import { SpotifyTrackId } from "../functions/data/spotify";

const comicNeue = Comic_Neue({
  subsets: ["latin"],
  display: "swap",
  weight: "400",
});

export type Props = {
  timeline: PlayerTimeline;
  lastAdded?: SpotifyTrackId;
  onPositionGuess?: (positionGuess: number) => void;
};

export const Timeline = (props: Props) => {
  const onPositionGuess = props.onPositionGuess;
  const [hasHighlightedLastAdded, setHasHighlightedLastAdded] = useState(true);

  useEffect(() => {
    if (!props.lastAdded) {
      return;
    }

    setHasHighlightedLastAdded(false);
    setTimeout(() => setHasHighlightedLastAdded(true), 1500);
  }, [props.lastAdded]);

  const timelineElems = props.timeline.flatMap((entry, timelinePosition) => {
    const isLast = timelinePosition === props.timeline.length - 1;
    const nextElem = props.timeline[timelinePosition + 1];
    const isSameAsNext =
      typeof nextElem !== "undefined" && entry.year === nextElem.year;
    return [
      <li
        key={entry.spotifyId}
        className={`border-4 border-solid border-yellow-100 rounded-xl px-7 py-5 text-slate-50 transition-shadow ease-in ${props.lastAdded === entry.spotifyId && !hasHighlightedLastAdded ? "shadow-lg shadow-yellow-500 duration-300" : "duration-700 "}`}
      >
        <dl className="flex flex-col">
          <dt className="sr-only">Year</dt>
          <dd className="text-5xl font-bold p-2">{entry.year}</dd>
          <dt className="sr-only">Artist</dt>
          <dd className="font-bold text-lg">{entry.artist}</dd>
          <dt className="sr-only">Title</dt>
          <dd className="text-md">{entry.title}</dd>
        </dl>
      </li>,
      <li key={`post_${entry.spotifyId}`} className="flex justify-stretch">
        {typeof onPositionGuess === "function" && !isSameAsNext ? (
          <button
            onClick={() => onPositionGuess(timelinePosition + 1)}
            className="group relative flex-grow flex flex-col items-center focus-visible:outline-none"
          >
            <span
              aria-hidden
              className={`hidden group-focus-visible:block text-yellow-100 text-5xl font-bold absolute left-0 top-1/2 -translate-y-1/2`}
            >
              ［
            </span>
            <Stop ends={isLast ? "end" : "both"} />
            <span
              aria-hidden
              className={`hidden group-focus-visible:block text-yellow-100 text-5xl font-bold absolute right-0 top-1/2 -translate-y-1/2`}
            >
              ］
            </span>
          </button>
        ) : (
          <span className="flex-grow flex flex-col items-center">
            <Stop ends="none" />
          </span>
        )}
      </li>,
    ];
  });

  return (
    <div className="flex flex-col">
      {typeof onPositionGuess === "function" && props.timeline.length === 1 && (
        <p
          className={`${comicNeue.className} text-xl text-start flex flex-col items-center gap-4 max-w-96`}
        >
          <span>
            It&apos;s your turn! Pick this if you think the current song is from{" "}
            <i>before</i> {props.timeline[0].year}&hellip;
          </span>
          <ArrowDown className="stroke-white -translate-x-6" />
        </p>
      )}
      <ol className="max-w-full flex flex-col justify-stretch items-stretch text-center">
        <li className="flex justify-stretch">
          {typeof onPositionGuess === "function" ? (
            <button
              onClick={() => onPositionGuess(0)}
              className="group relative flex-grow flex flex-col items-center focus-visible:outline-none"
            >
              <span
                aria-hidden
                className="hidden group-focus-visible:block text-yellow-100 text-5xl font-bold absolute left-0 top-1/2 -translate-y-1/2"
              >
                ［
              </span>
              <Stop ends="start" />
              <span
                aria-hidden
                className="hidden group-focus-visible:block text-yellow-100 text-5xl font-bold absolute right-0 top-1/2 -translate-y-1/2"
              >
                ］
              </span>
            </button>
          ) : (
            <span className="flex-grow flex flex-col items-center">
              <Stop ends="none" />
            </span>
          )}
        </li>
        {timelineElems}
      </ol>
      {typeof onPositionGuess === "function" && props.timeline.length === 1 && (
        <p
          className={`${comicNeue.className} text-xl text-end flex flex-col items-center gap-4 max-w-96`}
        >
          <ArrowUp className="stroke-white translate-x-4 -scale-x-100" />
          <span>
            &hellip;or this if you think it&apos;s from <i>after</i>{" "}
            {props.timeline[0].year}.
          </span>
        </p>
      )}
    </div>
  );
};

type StopProps = {
  ends: "none" | "start" | "end" | "both";
};

const Stop = (props: StopProps) => {
  const width = 80;
  const height = 100;
  const strokeWidth = 5;
  const strokeClass = "stroke-yellow-100 group-hover:stroke-yellow-400";
  const fillClass = "fill-orange-300 group-hover:fill-orange-400";

  const stopRadius = width / 4;

  if (props.ends === "none") {
    return (
      <svg width={width} height={height} viewBox={`0 0 ${width} ${height}`}>
        <g style={{ strokeWidth: `${strokeWidth}px` }}>
          <path
            d={`M ${width / 2},0 V ${height}`}
            className={`${strokeClass}`}
          />
        </g>
      </svg>
    );
  }

  if (props.ends === "start") {
    return (
      <svg
        width={width}
        height={height / 2 + stopRadius * 2}
        viewBox={`0 0 ${width} ${height / 2 + stopRadius * 2}`}
      >
        <g style={{ strokeWidth: `${strokeWidth}px` }}>
          <path
            d={`M ${width / 2},${stopRadius * 2 + strokeWidth} V ${height / 2 + stopRadius * 2}`}
            className={`${strokeClass}`}
          />
          <circle
            className={`${strokeClass} ${fillClass} transition-transform group-hover:scale-150 group-hover:-translate-x-1/4  group-hover:-translate-y-1/4`}
            cx={width / 2}
            cy={0 + stopRadius * 2}
            r={stopRadius - strokeWidth}
          />
        </g>
      </svg>
    );
  }

  if (props.ends === "end") {
    return (
      <svg
        width={width}
        height={height / 2 + stopRadius * 2}
        viewBox={`0 0 ${width} ${height / 2 + stopRadius * 2}`}
      >
        <g style={{ strokeWidth: `${strokeWidth}px` }}>
          <path
            d={`M ${width / 2},0 V ${height / 2 + stopRadius - strokeWidth}`}
            className={`${strokeClass}`}
          />
          <circle
            className={`${strokeClass} ${fillClass} transition-transform group-hover:scale-150 group-hover:-translate-x-1/4  group-hover:-translate-y-1/4`}
            cx={width / 2}
            cy={height / 2}
            r={stopRadius - strokeWidth}
          />
        </g>
      </svg>
    );
  }

  return (
    <svg width={width} height={height} viewBox={`0 0 ${width} ${height}`}>
      <g style={{ strokeWidth: `${strokeWidth}px` }}>
        <path d={`M ${width / 2},0 V ${height}`} className={`${strokeClass}`} />
        <circle
          className={`${strokeClass} ${fillClass} transition-transform group-hover:scale-150 group-hover:-translate-x-1/4  group-hover:-translate-y-1/4`}
          cx={width / 2}
          cy={height / 2}
          r={stopRadius - strokeWidth}
        />
      </g>
    </svg>
  );
};
