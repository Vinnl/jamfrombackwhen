import { Lobster } from "next/font/google";
import { SVGAttributes } from "react";

const lobster = Lobster({
  subsets: ["latin"],
  display: "swap",
  weight: "400",
});

export const Logo = (props: SVGAttributes<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="505.514"
      height="374.456"
      version="1.1"
      viewBox="0 0 520 400"
      aria-label="Jam from back when"
      {...props}
    >
      <g
        strokeWidth="3.78"
        fontStretch="normal"
        fontStyle="normal"
        fontVariant="normal"
        fontWeight="normal"
        textAnchor="middle"
        transform="translate(0 -15)"
      >
        <text
          xmlSpace="preserve"
          className={`${lobster.className} text-center stroke-yellow-950 fill-yellow-500`}
          x="209.925"
          y="200.286"
          fill="none"
          fontSize="240"
          strokeWidth={6}
        >
          <tspan x="209.925" y="200.286" fontSize="240">
            Jam
          </tspan>
        </text>{" "}
        <text
          xmlSpace="preserve"
          className={`${lobster.className} text-center fill-orange-700`}
          x="175.885"
          y="266.897"
          fill="#000"
          fontSize="80"
        >
          <tspan x="175.885" y="266">
            from
          </tspan>
        </text>{" "}
        <text
          xmlSpace="preserve"
          className={`${lobster.className} text-center fill-orange-700`}
          x="376.579"
          y="270.977"
          fill="#000"
          fontSize="80"
        >
          <tspan x="376.579" y="267">
            back
          </tspan>
        </text>{" "}
        <text
          xmlSpace="preserve"
          className={`${lobster.className} text-center stroke-orange-300 fill-orange-600`}
          x="307.558"
          y="393.543"
          fontSize="200"
          strokeWidth={6}
        >
          <tspan x="307.558" y="393.543" fontSize="200">
            when
          </tspan>
        </text>
      </g>
    </svg>
  );
};
