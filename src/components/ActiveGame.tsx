import { CSSProperties } from "react";
import { Comic_Neue } from "next/font/google";
import { WINNING_TIMELINE_LENGTH } from "../constants";
import {
  GameState,
  PlayerId,
  getCurrentPlayerId,
  getLastCorrectTrack,
  getTimelineAtRound,
  hasFinished,
} from "../functions/gamestate";
import { Timeline } from "./Timeline";
import { GameStats } from "./GameStats";

const comicNeue = Comic_Neue({
  subsets: ["latin"],
  display: "swap",
  weight: "400",
});

type ActivePlayerProps = {
  playerId: PlayerId;
  onPositionGuess: (positionGuess: number) => void;
  onPlayAgain: () => void;
};

type SpectatingPlayerProps = {
  playerId: PlayerId;
  onRejoinGame: () => void;
};

export type Props = {
  gameState: GameState;
  onQuitGame?: () => void;
  onEndGame?: () => void;
} & ({ playerId?: undefined } | SpectatingPlayerProps | ActivePlayerProps);

export const ActiveGame = (props: Props) => {
  const currentPlayerId = getCurrentPlayerId(props.gameState);

  const activePlayerTimeline = hasActivePlayer(props)
    ? getTimelineAtRound(
        props.gameState,
        props.playerId,
        props.gameState.completedRound + 1,
      )
    : undefined;

  if (
    // There's no active player...
    typeof activePlayerTimeline === "undefined" ||
    // ...or the active player has left the game:
    (hasActivePlayer(props) &&
      !props.gameState.players[props.playerId].isPresent)
  ) {
    // Spectator
    const currentPlayerTimeline = getTimelineAtRound(
      props.gameState,
      currentPlayerId,
      props.gameState.completedRound + 1,
    );

    return (
      <>
        <p className={`${comicNeue.className} text-xl`}>
          You&apos;re watching an in-progress game — this is{" "}
          {props.gameState.players[currentPlayerId].name}&apos;s timeline.
        </p>
        <GameStats
          gameState={props.gameState}
          activePlayerId={
            hasActivePlayer(props) || hasSpectatingPlayer(props)
              ? props.playerId
              : undefined
          }
          onPlayAgain={
            hasActivePlayer(props) ? () => props.onPlayAgain() : undefined
          }
          onEndGame={props.onEndGame}
          onRejoinGame={
            hasSpectatingPlayer(props) ? props.onRejoinGame : undefined
          }
        />
        <Timeline
          timeline={currentPlayerTimeline}
          lastAdded={getLastCorrectTrack(props.gameState)?.spotifyId}
        />
      </>
    );
  }

  if (currentPlayerId !== props.playerId) {
    // TODO: Allow guessing song if !!props.player, otherwise spectator mode.
  }

  return (
    <>
      <GameStats
        gameState={props.gameState}
        activePlayerId={hasActivePlayer(props) ? props.playerId : undefined}
        onPlayAgain={
          hasActivePlayer(props) ? () => props.onPlayAgain() : undefined
        }
        onQuitGame={hasActivePlayer(props) ? props.onQuitGame : undefined}
        onEndGame={props.onEndGame}
      />
      <Timeline
        timeline={activePlayerTimeline}
        onPositionGuess={
          currentPlayerId === props.playerId &&
          !hasFinished(props.gameState) &&
          hasActivePlayer(props)
            ? (positionGuess) => props.onPositionGuess(positionGuess)
            : undefined
        }
        lastAdded={getLastCorrectTrack(props.gameState)?.spotifyId}
      />
    </>
  );
};

function hasActivePlayer(props: Props): props is Props & ActivePlayerProps {
  return (
    typeof (props as ActivePlayerProps).playerId !== "undefined" &&
    typeof (props as ActivePlayerProps).onPositionGuess === "function"
  );
}

function hasSpectatingPlayer(
  props: Props,
): props is Props & SpectatingPlayerProps {
  return (
    typeof (props as SpectatingPlayerProps).playerId !== "undefined" &&
    typeof (props as SpectatingPlayerProps).onRejoinGame === "function"
  );
}
