"use client";

import { useEffect, useRef, useState } from "react";
import { SpotifyTrackId, pause, play } from "../functions/data/spotify";
import Script from "next/script";

export type Props = {
  authToken: string;
  currentTrack?: SpotifyTrackId;
  onInitialisationError?: () => void;
  onAuthError?: () => void;
};

export const SpotifyPlayer = (props: Props) => {
  const [playbackController, setPlaybackController] =
    useState<SpotifyPlaybackController>();
  const [playbackControllerState, setPlaybackControllerState] =
    useState<SpotifyPlaybackState>();
  const [deviceId, setDeviceId] = useState<SpotifyDeviceId>();
  /**
   * This needs to be a ref so the `getOAuthToken` callback, which is created in
   * the first render, can reference values set in later renders.
   */
  const authTokenRef = useRef(props.authToken);

  useEffect(() => {
    authTokenRef.current = props.authToken;
  }, [props.authToken]);

  useEffect(() => {
    window.onSpotifyWebPlaybackSDKReady = () => {
      if (!window.Spotify) {
        return;
      }
      const player = new window.Spotify.Player({
        name: "Jam from back when",
        getOAuthToken: (cb) => {
          return cb(authTokenRef.current);
        },
      });

      player.addListener("ready", ({ device_id }) => {
        setDeviceId(device_id);
      });

      player.addListener("not_ready", ({ device_id }) => {
        console.log("Device ID has gone offline", device_id);
      });

      player.addListener("player_state_changed", (state) => {
        setPlaybackControllerState(state);
      });

      player.addListener("initialization_error", () => {
        props.onInitialisationError?.();
      });
      player.addListener("account_error", (e) => {
        console.log("There was an error with your account:", e);
      });
      player.addListener("authentication_error", (e) => {
        props.onAuthError?.();
      });
      player.addListener("autoplay_failed", () => {
        console.log("Autoplay error");
      });

      player.connect().then(() => {
        setPlaybackController(player);
      });
    };
  }, [props]);

  const currentlyPlayingId =
    playbackControllerState?.track_window.current_track.id;

  useEffect(() => {
    if (!deviceId) {
      return;
    }

    if (!props.currentTrack) {
      pause(deviceId, authTokenRef.current);
      // TODO: Is this a better method (no HTTP requests before pausing),
      //       or will it trigger spurious extra pauses due to the effect
      //       dependencies?
      // playbackController?.pause();
      return;
    }

    if (
      typeof currentlyPlayingId === "undefined" ||
      currentlyPlayingId !== props.currentTrack
    ) {
      play(props.currentTrack, deviceId, authTokenRef.current);
    }
  }, [currentlyPlayingId, deviceId, props.currentTrack]);

  return (
    <>
      <Script
        src="https://sdk.scdn.co/spotify-player.js"
        strategy="lazyOnload"
      />
    </>
  );
};
