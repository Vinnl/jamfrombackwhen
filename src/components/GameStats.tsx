import { CSSProperties } from "react";
import { Comic_Neue } from "next/font/google";
import { MdClose } from "react-icons/md";
import { WINNING_TIMELINE_LENGTH } from "../constants";
import {
  GameState,
  PlayerId,
  getCurrentPlayerId,
  getTimelineAtRound,
  getWinner,
} from "../functions/gamestate";
import { ArrowUp } from "./Arrows";

const comicNeue = Comic_Neue({
  subsets: ["latin"],
  display: "swap",
  weight: "400",
});

export type Props = {
  gameState: GameState;
  activePlayerId?: PlayerId;
  onPlayAgain?: () => void;
  onRejoinGame?: () => void;
  onQuitGame?: () => void;
  onEndGame?: () => void;
};

export const GameStats = (props: Props) => {
  const winner = getWinner(props.gameState);
  const onPlayAgain = props.onPlayAgain;
  const onQuitGame = props.onQuitGame;
  const onEndGame = props.onEndGame;
  const winNotice =
    winner === null ? null : winner === props.activePlayerId ? (
      <div className="p-5 rounded-md text-lg text-green-900 bg-green-200 border-2 border-solid border-green-800">
        You won the game! 🎉
        {typeof onPlayAgain === "function" && (
          <>
            {" "}
            <button
              onClick={() => onPlayAgain()}
              className="underline  hover:text-slate-600 rounded-sm"
            >
              Play again?
            </button>
          </>
        )}
      </div>
    ) : (
      <div className="p-5 rounded-md text-lg text-slate-800 bg-yellow-300">
        <b>{props.gameState.players[winner].name}</b> won the game!
        {typeof onPlayAgain === "function" && (
          <>
            {" "}
            <button
              onClick={() => onPlayAgain()}
              className="underline  hover:text-slate-600 rounded-sm"
            >
              Play again?
            </button>
          </>
        )}
      </div>
    );

  return (
    <div className="flex flex-col gap-4 max-w-full">
      <div className="flex flex-col gap-2">
        <dl className="flex flex-col text-lg text-slate-700 bg-yellow-300 max-w-full w-96 border-4 border-solid border-yellow-300 rounded-md pt-0 p-2 gap-1">
          {Object.entries(props.gameState.players).map(([playerId, player]) => {
            const isActivePlayer = props.activePlayerId === playerId;
            const playerTimeline = getTimelineAtRound(
              props.gameState,
              playerId,
              props.gameState.completedRound + 1,
            );
            const progressPercentage =
              (playerTimeline.length / WINNING_TIMELINE_LENGTH) * 100;
            const onRejoinGame = props.onRejoinGame;

            return (
              <div key={playerId}>
                <dt
                  className={`${isActivePlayer ? "font-bold" : ""} flex items-center gap-1 py-1`}
                >
                  {getCurrentPlayerId(props.gameState) === playerId ? (
                    <span aria-label="Current turn:">►</span>
                  ) : null}{" "}
                  <span
                    className={!player.isPresent ? "line-through" : ""}
                    aria-label={
                      !player.isPresent
                        ? `Disconnected player: ${player.name}`
                        : undefined
                    }
                  >
                    {player.name}
                  </span>
                  {isActivePlayer && typeof onRejoinGame === "function" && (
                    <>
                      {" "}
                      <button
                        onClick={() => onRejoinGame()}
                        className="font-normal"
                      >
                        (rejoin)
                      </button>
                    </>
                  )}
                </dt>
                <dd
                  className={`flex justify-center items-center px-3 py-1 font-bold rounded bg-slate-700 text-white bg-gradient-to-r from-orange-700 from-50% to-orange-950 to-51%`}
                  style={
                    {
                      "--tw-gradient-from-position": `${progressPercentage}%`,
                      "--tw-gradient-to-position": `calc(${progressPercentage}% + 1px)`,
                    } as CSSProperties
                  }
                >
                  {playerTimeline.length}&nbsp;/&nbsp;{WINNING_TIMELINE_LENGTH}
                </dd>
              </div>
            );
          })}
        </dl>
        <div className="flex justify-end items-start gap-5 max-w-full w-96 text-sm">
          {typeof onQuitGame === "function" && (
            <button
              onClick={() => onQuitGame()}
              className="flex-shrink-0 flex items-center gap-1 text-red-300 font-bold ps-2 hover:underline hover:text-red-500"
            >
              Leave game
              <MdClose aria-hidden className="text-xl" />
            </button>
          )}
          {typeof onEndGame === "function" && (
            <button
              onClick={() => onEndGame()}
              className="flex-shrink-0 flex items-center gap-1 text-red-300 font-bold ps-2 hover:underline hover:text-red-500"
            >
              End game
              <MdClose aria-hidden className="text-xl" />
            </button>
          )}
        </div>
      </div>
      {props.gameState.completedRound === -1 &&
        typeof props.activePlayerId !== "undefined" &&
        getCurrentPlayerId(props.gameState) !== props.activePlayerId && (
          <p
            className={`${comicNeue.className} text-xl text-end flex flex-col items-center gap-4`}
          >
            <ArrowUp className="stroke-white translate-x-4 -scale-x-100" />
            <span>
              Please wait for{" "}
              <b>
                {
                  props.gameState.players[getCurrentPlayerId(props.gameState)]
                    .name
                }
              </b>{" "}
              to finish their turn.
            </span>
          </p>
        )}
      {winNotice}
    </div>
  );
};
