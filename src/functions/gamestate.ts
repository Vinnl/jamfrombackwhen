import type Peer from "peerjs";
import { WINNING_TIMELINE_LENGTH } from "../constants";
import type { SpotifyTrackId } from "./data/spotify";
import type { PlaylistData } from "../components/PlaylistPicker";

export type TrackOption = {
  year: number;
  spotifyId: SpotifyTrackId;
  artist: string;
  title: string;
};

export type PlayerName = string;
export type Player = {
  name: PlayerName;
  seed: TrackOption;
  isPresent: boolean;
  peerId: Peer["id"];
};

type Uuid = string;
export type Players = Record<Uuid, Player>;
export type PlayerId = keyof Players;

export type Response = {
  // A guess is `null` if the player has disconnected or left:
  positionGuess: number | null;
};

export type Round = number;

export type GameState = {
  started: true;
  players: Players;
  completedRound: Round;
  tracks: TrackOption[];
  responses: Response[];
  playlist: PlaylistData;
};

export type PlayerTimeline = TrackOption[];

export function getCurrentPlayerId(
  state: GameState,
  round = state.completedRound + 1,
): PlayerId {
  const playerIds = Object.keys(state.players);
  return playerIds[round % playerIds.length];
}

export function getCurrentTrack(
  state: GameState,
  round = state.completedRound + 1,
): TrackOption | null {
  if (hasWinner(state)) {
    return state.tracks[round - 1] ?? null;
  }
  return state.tracks[round] ?? null;
}

export function getPlayerResponses(
  state: GameState,
  playerId: PlayerId,
): Response[] {
  const playerIds = Object.keys(state.players);
  const playerIndex = playerIds.indexOf(playerId);
  return state.responses.filter(
    (_response, round) => round % (playerIds.length + playerIndex) === 0,
  );
}

/**
 *
 * @param currentTrack
 * @param timeline
 * @param positionGuess 0 is at the start of the timeline, 1 is after the first element.
 * @returns
 */
export function isGuessCorrect(
  currentTrack: TrackOption,
  timeline: PlayerTimeline,
  positionGuess: number | null,
): boolean {
  if (positionGuess === null) {
    return false;
  }
  if (positionGuess === 0) {
    return currentTrack.year <= timeline[0].year;
  }
  if (positionGuess === timeline.length) {
    return currentTrack.year >= timeline[timeline.length - 1].year;
  }

  return (
    currentTrack.year >= timeline[positionGuess - 1].year &&
    currentTrack.year <= timeline[positionGuess].year
  );
}

export function getTimelineAtRound(
  state: GameState,
  playerId: PlayerId,
  round: Round,
): PlayerTimeline {
  if (round === -1) {
    return [state.players[playerId].seed];
  }

  const prevTimeline = getTimelineAtRound(state, playerId, round - 1);
  const currentPositionGuess = state.responses[round]?.positionGuess;
  const isThisPlayersTurn = getCurrentPlayerId(state, round) === playerId;
  if (
    !isThisPlayersTurn ||
    typeof currentPositionGuess === "undefined" ||
    currentPositionGuess === null ||
    !isGuessCorrect(state.tracks[round], prevTimeline, currentPositionGuess)
  ) {
    return prevTimeline;
  }

  return [
    ...prevTimeline.slice(0, currentPositionGuess),
    state.tracks[round],
    ...prevTimeline.slice(currentPositionGuess),
  ];
}

export function getCollectiveTimelineAtRound(
  state: GameState,
  round: Round,
): PlayerTimeline {
  const tracks = Object.values(state.players)
    .map((player) => player.seed)
    .concat(state.tracks.slice(0, state.completedRound + 1));
  tracks.sort((trackA, trackB) => trackA.year - trackB.year);
  return tracks;
}

export function getWinner(state: GameState): null | PlayerId {
  const playerIds = Object.keys(state.players);
  // `state.completedRound + 1` because, in single-player mode, we determine the
  // winner before completing the final round.
  // `WINNING_TIMELINE_LENGTH - 1` because the first timeline entry is a given,
  // so the players theoretically need one round less to get the full timeline:
  if (state.completedRound + 1 < WINNING_TIMELINE_LENGTH - 1) {
    // No one can have won yet if there have been fewer rounds that the number
    // of songs (+1 for the seed) needed to fill a timeline:
    return null;
  }

  return (
    playerIds.find(
      (playerId) =>
        getTimelineAtRound(state, playerId, state.completedRound + 1).length ===
        WINNING_TIMELINE_LENGTH,
    ) ?? null
  );
}

export function hasWinner(state: GameState): boolean {
  return getWinner(state) !== null;
}

export function hasActivePlayers(state: GameState): boolean {
  return (
    Object.values(state.players).filter((player) => player.isPresent).length > 0
  );
}

export function hasFinished(state: GameState): boolean {
  return hasWinner(state) || !hasActivePlayers(state);
}

export function getLastCorrectTrack(
  state: GameState,
  round = state.completedRound,
): TrackOption | undefined {
  if (round === -1) {
    return undefined;
  }

  if (hasWinner(state)) {
    return state.tracks[round];
  }

  const currentPlayerId = getCurrentPlayerId(state, round);
  const currentPlayerTimeline = getTimelineAtRound(
    state,
    currentPlayerId,
    round,
  );
  const currentTrack = getCurrentTrack(state, round);
  if (currentTrack === null) {
    return undefined;
  }
  if (
    isGuessCorrect(
      currentTrack,
      currentPlayerTimeline,
      state.responses[round].positionGuess,
    )
  ) {
    return currentTrack;
  }

  return getLastCorrectTrack(state, round - 1);
}

export function isCurrentPlayerPresent(state: GameState): boolean {
  return state.players[getCurrentPlayerId(state)].isPresent;
}

export function updateWithGuess(
  prev: GameState,
  positionGuess: number | null,
): GameState {
  const updatedGameState = {
    ...prev,
    responses: [
      ...prev.responses,
      {
        positionGuess: positionGuess,
      },
    ],
    // TODO: Allow guessing the song if the guess is wrong:
    completedRound: prev.completedRound + 1,
  };
  // If the next player is not present (and there *is* a next player, i.e. the
  // game hasn't ended yet), respond `null` for them (recursively, until the
  // next present player):
  return isCurrentPlayerPresent(updatedGameState) ||
    hasFinished(updatedGameState)
    ? updatedGameState
    : updateWithGuess(updatedGameState, null);
}

export function markPlayerAsDisconnected(
  prev: GameState,
  playerId: PlayerId,
): GameState {
  const currentPlayerId = getCurrentPlayerId(prev);
  const updatedState = {
    ...prev,
    players: {
      ...prev.players,
      [playerId]: {
        ...prev.players[playerId],
        isPresent: false,
      },
    },
  };

  // Move to the next round if the player whose turn it is disconnects
  return currentPlayerId === playerId && !hasFinished(updatedState)
    ? updateWithGuess(updatedState, null)
    : updatedState;
}

export function markPlayerAsRejoined(
  prev: GameState,
  playerId: PlayerId,
): GameState {
  const existingPlayerData = prev.players[playerId];
  if (!existingPlayerData) {
    return prev;
  }
  const updatedState = {
    ...prev,
    players: {
      ...prev.players,
      [playerId]: {
        ...existingPlayerData,
        isPresent: true,
      },
    },
  };

  return updatedState;
}
