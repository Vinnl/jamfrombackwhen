import { Account } from "next-auth";
import { JWT } from "next-auth/jwt";

export type SpotifyTrackId = string;
export type SpotifyArtistId = string;
export type SpotifyAlbumId = string;
export type SpotifyPlaylistId = string;
export type SpotifySnapshotId = string;
export type SpotifyUserId = string;

type SpotifyAlbumType = "album" | "single" | "compilation";
type SpotifyReleaseDatePrecision = "year" | "month" | "day";

// https://developer.spotify.com/documentation/web-api/concepts/playlists#using-playlist-images
// > The images array that’s returned can vary depending on how many tracks are
// > in the playlist, and if the playlist has been manually “annotated”. The
// > images array can contain:
export type SpotifyImageObjectArray =
  // > Nothing, if the playlist has no tracks and is empty,
  | []
  // > An album cover of size 640×640, if the playlist contains 1 to 3 tracks or
  // > has tracks from less than 4 different albums,
  | [{ url: string; height: 640; width: 640 }]
  // > Three mosaic images of size 640×640, 300×300, and 60×60, if the playlist
  // > contains tracks from 4 or more albums,
  | [
      { url: string; height: 640; width: 640 },
      { url: string; height: 300; width: 300 },
      { url: string; height: 60; width: 60 },
    ]
  // > A single custom image in various sizes, if the playlist image has been
  // > set manually — for example, for some curated playlists.
  | Array<{ url: string; height: number; width: number }>;

type SpotifySimplifiedPlaylistObject = {
  id: SpotifyPlaylistId;
  collaborative: boolean;
  name: string;
  description: string;
  type: "playlist";
  public: boolean;
  snapshot_id: SpotifySnapshotId;
  images: SpotifyImageObjectArray;
  tracks: {
    href: string;
    total: number;
  };
  owner: {
    type: "user";
    display_name: string;
    id: SpotifyUserId;
  };
};

type SpotifyPlaylistTrackObject = {
  is_local: boolean;
  track: {
    duration_ms: number;
    id: SpotifyTrackId;
    is_playable: boolean;
    name: string;
    is_local: boolean;
    album: {
      album_type: SpotifyAlbumType;
      name: string;
      release_date: string;
      release_date_precision: SpotifyReleaseDatePrecision;
    };
    artists: Array<{
      id: SpotifyArtistId;
      name: string;
    }>;
  };
};

async function spotifyFetch(
  endpoint: string,
  accessToken: Account["access_token"],
  options?: Parameters<typeof fetch>[1],
) {
  const headers = new Headers(options?.headers);
  headers.set("Authorization", `Bearer ${accessToken}`);
  const response = await fetch(`https://api.spotify.com/v1${endpoint}`, {
    ...options,
    headers: headers,
  });
  if (!response.ok && response.status === 502) {
    // It looks like Spotify sometimes just fails with the following body:
    // > {"error": {"status": 502, "message": "Error while loading resource" } }
    // Let's just retry once and see if that fixes it:
    return fetch(`https://api.spotify.com/v1${endpoint}`, {
      ...options,
      headers: headers,
    });
  }
  return response;
}

/** https://developer.spotify.com/documentation/web-api/reference/get-current-users-profile */
export type SpotifyGetProfileResponseBody = {
  /** Requires user-read-private scope */
  country: string;
  // display_name: string | null;
  display_name: string;
  /** Note: unverified!. Requires user-read-email scope. */
  email: string;
  /** Requires user-read-private scope */
  explicit_content: {
    filter_enabled: boolean;
    filter_locked: boolean;
  };
  external_urls: {
    spotify: string;
  };
  followers: {
    href: null;
    total: number;
  };
  href: string;
  id: SpotifyUserId;
  images: SpotifyImageObjectArray;
  /** Requires user-read-private scope */
  product: "premium" | "free" | "open";
  type: "user";
  uri: string;
};

/** https://developer.spotify.com/documentation/web-api/reference/get-playlist */
export type SpotifyGetSearchResponseBody = {
  tracks: {
    limit: number;
    offset: number;
    total: number;
    items: Array<{
      duration_ms: number;
      id: SpotifyTrackId;
      is_playable: boolean;
      name: string;
      is_local: boolean;
      type: "track";
      popularity: number;
      album: {
        album_type: SpotifyAlbumType;
        name: string;
        release_date: string;
        release_date_precision: SpotifyReleaseDatePrecision;
      };
      artists: Array<{
        id: SpotifyArtistId;
        name: string;
      }>;
    }>;
  };
  artists: {
    limit: number;
    offset: number;
    total: number;
    items: Array<{
      id: SpotifyArtistId;
      name: string;
      type: "artist";
      popularity: number;
    }>;
  };
  albums: {
    limit: number;
    offset: number;
    total: number;
    items: Array<{
      album_type: SpotifyAlbumType;
      total_tracks: number;
      id: SpotifyAlbumId;
      name: string;
      type: "album";
      release_date: string;
      release_date_precision: SpotifyReleaseDatePrecision;
      artists: Array<{
        id: SpotifyArtistId;
        name: string;
      }>;
    }>;
  };
  playlists: {
    limit: number;
    offset: number;
    total: number;
    items: Array<SpotifySimplifiedPlaylistObject>;
  };
};
export async function fetchSearchResults(
  query: string,
  type: Array<"album" | "artist" | "playlist" | "track">,
  accessToken: Account["access_token"],
): Promise<SpotifyGetSearchResponseBody> {
  const firstResponse = await spotifyFetch(
    `/search/?limit=20&q=${encodeURIComponent(query)}&type=${type.join(",")}`,
    accessToken,
  );
  const data = (await firstResponse.json()) as SpotifyGetSearchResponseBody;
  return data;
}

export async function fetchArtistData(
  artistId: SpotifyArtistId,
  accessToken: Account["access_token"],
) {
  return spotifyFetch(`/artists/${artistId}`, accessToken);
}

/** https://developer.spotify.com/documentation/web-api/reference/get-playlist */
export type SpotifyGetPlaylistResponseBody = {
  id: SpotifyPlaylistId;
  name: string;
  images: SpotifyImageObjectArray;
  tracks: {
    limit: number;
    offset: number;
    total: number;
    items: Array<{
      is_local: boolean;
      track: {
        duration_ms: number;
        id: SpotifyTrackId;
        is_playable: boolean;
        name: string;
        is_local: boolean;
        album: {
          album_type: SpotifyAlbumType;
          name: string;
          release_date: string;
          release_date_precision: SpotifyReleaseDatePrecision;
        };
        artists: Array<{
          id: SpotifyArtistId;
          name: string;
        }>;
      };
    }>;
  };
};
export async function fetchPlaylistData(
  playlistId: SpotifyPlaylistId,
  accessToken: Account["access_token"],
): Promise<SpotifyGetPlaylistResponseBody> {
  const firstResponse = await spotifyFetch(
    `/playlists/${playlistId}/?limit=100`,
    accessToken,
  );
  const data = (await firstResponse.json()) as SpotifyGetPlaylistResponseBody;
  return data;
}

export function getAlbumYear(album: {
  release_date: string;
  release_date_precision: SpotifyReleaseDatePrecision;
}): number {
  const dateParts = album.release_date
    .split("-")
    .map((part) => Number.parseInt(part, 10));
  return dateParts[0];
}

/** https://developer.spotify.com/documentation/web-api/reference/get-playlists-tracks */
export type SpotifyGetPlaylistTracksResponseBody = {
  limit: number;
  offset: number;
  total: number;
  items: Array<SpotifyPlaylistTrackObject>;
};
export async function fetchPlaylistTracks(
  playlistId: SpotifyPlaylistId,
  accessToken: Account["access_token"],
): Promise<SpotifyGetPlaylistTracksResponseBody> {
  const firstResponse = await spotifyFetch(
    `/playlists/${playlistId}/tracks?limit=100`,
    accessToken,
  );
  const data =
    (await firstResponse.json()) as SpotifyGetPlaylistTracksResponseBody;
  let lastData = data;
  while (lastData.offset + lastData.limit < lastData.total) {
    const nextResponse = await spotifyFetch(
      `/playlists/${playlistId}/tracks/?limit=100&offset=${lastData.offset + lastData.limit}`,
      accessToken,
    );
    lastData =
      (await nextResponse.json()) as SpotifyGetPlaylistTracksResponseBody;
    data.items = data.items.concat(lastData.items);
    data.limit += lastData.limit;
  }
  return data;
}

/**
 * @deprecated API appears to be broken; results in a Bad Gateway error.
 */
export async function addToQueue(
  uri: string,
  deviceId: SpotifyDeviceId,
  accessToken: Account["access_token"],
) {
  return spotifyFetch(
    `/me/player/queue?uri=${encodeURIComponent(uri)}&device_id=${encodeURIComponent(deviceId)}`,
    accessToken,
    {
      method: "POST",
    },
  );
}

type SpotifyPostPlayRequestBody = {
  uris?: `spotify:track:${SpotifyTrackId}`[];
  context_uri?: string;
  /** Only available when context_uri corresponds to an album or playlist object */
  offset?:
    | {
        position: number;
      }
    | { uri: string };
  position_ms?: number;
};
export async function play(
  trackId: SpotifyTrackId,
  deviceId: SpotifyDeviceId,
  accessToken: Account["access_token"],
) {
  const body: SpotifyPostPlayRequestBody = {
    uris: [`spotify:track:${trackId}`],
  };
  return spotifyFetch(
    `/me/player/play?device_id=${encodeURIComponent(deviceId)}`,
    accessToken,
    {
      method: "PUT",
      body: JSON.stringify(body),
    },
  );
}

export async function pause(
  deviceId: SpotifyDeviceId,
  accessToken: Account["access_token"],
) {
  return spotifyFetch(
    `/me/player/pause?device_id=${encodeURIComponent(deviceId)}`,
    accessToken,
    {
      method: "PUT",
    },
  );
}

type RotatedSpotifyTokens = {
  refreshToken: JWT["refreshToken"];
  accessToken: JWT["accessToken"];
  /** Note: this is in seconds, not milliseconds, which is the granularity Spotify uses */
  expiresAt: number;
};
const rotationPromises: Record<
  NonNullable<JWT["refreshToken"]>,
  Promise<RotatedSpotifyTokens>
> = {};
export async function rotateRefreshToken(
  oldRefreshToken: NonNullable<JWT["refreshToken"]>,
): Promise<RotatedSpotifyTokens> {
  rotationPromises[oldRefreshToken] ??= new Promise(async (resolve, reject) => {
    const requestTime = Date.now();
    try {
      const response = await fetch("https://accounts.spotify.com/api/token", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams({
          grant_type: "refresh_token",
          refresh_token: oldRefreshToken,
          client_id: process.env.AUTH_SPOTIFY_ID!,
        }),
      });
      const data = (await response.json()) as SpotifyTokenRefreshResponse;
      if (!response.ok || hasError(data)) {
        throw data;
      }

      const rotatedTokens: RotatedSpotifyTokens = {
        accessToken: data.access_token,
        refreshToken: data.refresh_token,
        expiresAt: requestTime / 1000 + data.expires_in,
      };
      resolve(rotatedTokens);
    } catch (e) {
      reject(e);
    }
  });

  return rotationPromises[oldRefreshToken];
}

type SpotifyTokenRefreshSuccessResponse = {
  // See https://developer.spotify.com/documentation/web-api/tutorials/refreshing-tokens#response
  access_token: string;
  token_type: "Bearer";
  expires_in: 3600;
  refresh_token: string;
  scope: string;
};
type SpotifyTokenRefreshErrorResponse = {
  error: "invalid_grant";
  error_description: "Refresh token revoked";
};
type SpotifyTokenRefreshResponse =
  | SpotifyTokenRefreshSuccessResponse
  | SpotifyTokenRefreshErrorResponse;
function hasError(
  response: SpotifyTokenRefreshResponse,
): response is SpotifyTokenRefreshErrorResponse {
  return (
    typeof (response as SpotifyTokenRefreshErrorResponse).error === "string"
  );
}
