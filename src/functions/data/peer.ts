import type { GameState, PlayerId, PlayerName } from "../gamestate";

export type PeerData =
  | {
      type: "GameStateUpdate";
      value: GameState | null;
    }
  | {
      type: "PlayerJoin";
      value: {
        id: PlayerId;
        name: PlayerName;
      };
    }
  | {
      type: "PlayerLeave";
      value: PlayerId;
    }
  | {
      type: "PlayerRemoved";
      value: PlayerId;
    }
  | {
      type: "Guess";
      value: {
        playerId: PlayerId;
        round: number;
        positionGuess: number;
      };
    }
  | {
      type: "PlayAgain";
    };
